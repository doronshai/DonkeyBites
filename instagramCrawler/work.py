from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import time
import sys

# instantiate a chrome options object so you can set the size and headless preference
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=1920x1080")

accounttosearch = sys.argv[1]
username = sys.argv[2]
password = sys.argv[3]
chromepath = '/usr/bin/chromedriver'

class InstagramBot():
	def __init__(self, email, password):
		self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=chromepath)
		self.email = email
		self.password = password

	def signIn(self):
		self.browser.get('https://www.instagram.com/accounts/login/')
		time.sleep(4)
		emailInput = self.browser.find_elements_by_css_selector('form input')[0]
		passwordInput = self.browser.find_elements_by_css_selector('form input')[1]
		emailInput.send_keys(self.email)
		passwordInput.send_keys(self.password)
		passwordInput.send_keys(Keys.ENTER)
		time.sleep(2)

	def checkFollowers(self, accounttosearch):
		self.browser.get('https://www.instagram.com/'+accounttosearch)
		time.sleep(2)
		try:
			followers = self.browser.find_element_by_xpath('//a[@href="/'+accounttosearch+'/followers/"]').text
			following = self.browser.find_element_by_xpath('//a[@href="/'+accounttosearch+'/following/"]').text
			print("Followers "+followers.replace(' followers', ''))
			print("Following "+following.replace(' following', ''))
			self.browser.quit()
		except NoSuchElementException:
			followers = self.browser.find_elements_by_css_selector('span.g47SY')[1].text
			following = self.browser.find_elements_by_css_selector('span.g47SY')[2].text
			print("Followers "+followers.replace(' followers', ''))
			print("Following "+following.replace(' following', ''))
			self.browser.quit()

bot = InstagramBot(username, password)
#bot.signIn()
bot.checkFollowers(accounttosearch)
